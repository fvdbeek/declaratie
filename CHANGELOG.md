# Changelog

## [5.0.0] - 2022-04-18
- Use Downloads directory by default
- Improve Excel export
- Add unit tests
- Bugfixes

## [4.0.3] - 2022-04-06
- Fix "TypeError: re-matches must match against a string" on querying events

## [4.0.2] - 2022-04-06
- Fix UI quirks
- Improve responsiveness
- Many bug fixes

## [4.0.1] - 2022-03-30
- Minor UI changes

## [4.0.0] - 2022-03-29
- Disable Hermes Javascript engine because of incompatabilities
- Use re-frisk for profiling
- Use default Async Storage size
- Give calls their own local storage to improve speed
- Bug fixes

## [3.1.0] - 2022-03-13
- Monkey patch react-native-calendar-events to increase speed
- Fetch events incrementally
- Switch to transit-cljs for reading and writing EDN to increase speed
- Ditch own forked repositories
- Bug fixes

## [3.0.0] - 2022-01-09
- Use Hermes Javascript engine
- Replace react-native-fs with react-native-file-access because of Hermes compatibility
- Upgrade tick to 0.5.0-RC5
- Bug fixes

## [2.0.0] - 2021-12-06
- Upgrade react-native from 0.64.2 to 0.66.3
- Fix race condition when agenda tries to load new items before start and after end at the same time
- Fix bug in editing of Excel sheet

## [1.3.1] - 2021-12-02
- Wait for permissions before boot

## [1.3.0] - 2021-12-01
- Add instructions to grant permissions

## [1.2.0] - 2021-12-01
- Prompt for permissions
- Improve layout

## [1.1.1] - 2021-11-30
- Fix signing config

## [1.1.0] - 2021-11-29
- Fix scrolling of agenda and calendar
- Upgrade react-navigation to v6 (fixes https://github.com/software-mansion/react-native-screens/issues/1197)
- Upgrade all npm packages
- Add configuration for react-native-screens package
- Add debug symbols
- Add signing config
- Upgrade target SDK to 31 to meet Google Play's requirements

## [1.0.0] - 2021-11-04
- Initial Release
