(ns declaratie.time-test
  (:require
   [cljs.test :refer (deftest is testing run-tests)]
   [declaratie.time :as time]
   [tick.core :as t]))

(deftest test-round-up-to []
  (let [half-hour 30
        on-the-hour 946684800000 ; 2000-01-01T00:00:00Z
        on-the-half-hour 946686600000 ; 2000-01-01T00:30:00Z
        one-minute-to 946684740000 ; 1999-12-31T23:59:00Z
        one-minute-past 946684860000] ; 2000-01-01T00:01:00Z
    (is (= (time/round-up-to on-the-hour half-hour)
           on-the-hour))
    (is (= (time/round-up-to one-minute-to half-hour)
           on-the-hour))
    (is (= (time/round-up-to one-minute-past half-hour)
           on-the-half-hour))))

(deftest round-off-by-test []
  (let [quarter 15
        on-the-hour 946684800000 ; 2000-01-01T00:00:00Z
        seven-minutes-past 946685220000 ; 2000-01-01T00:07:00Z
        eight-minutes-past 946685280000 ; 2000-01-01T00:08:00Z
        quarter-past 946685700000] ; 2000-01-01T00:15:00Z
    (is (= (time/round-off-by seven-minutes-past quarter)
           on-the-hour))
    (is (= (time/round-off-by eight-minutes-past quarter)
           quarter-past))))

(deftest timestamp->instant-test []
  (let [epoch 0]
    (is (= (time/timestamp->instant epoch) (t/instant "1970-01-01T00:00:00Z")))))

(deftest instant->timestamp-test []
  (let [epoch 0]
    (is (= (time/instant->timestamp (t/instant "1970-01-01T00:00:00Z")) epoch))))

(deftest instant-timestamp-roundtrip []
  (is (= 0 (time/instant->timestamp (time/timestamp->instant 0)))))
