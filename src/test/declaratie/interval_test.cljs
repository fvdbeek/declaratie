(ns declaratie.interval-test
  (:require
   [cljs.test :refer (deftest is testing run-tests)]
   [declaratie.interval :as interval]
   [tick.core :as t]
   [tick.alpha.interval :as t.i]))

(deftest event->interval-test []
  (testing "event->interval"
    (let [empty-call {:duration 0 :timestamp "0"}
          call {:duration 1 :timestamp "0"}]
      (is
       (nil?
        (interval/call->interval empty-call)))
      (is
       (=
        (t/seconds (t/duration (interval/call->interval call)))
        (:duration call))))))

(deftest call->interval-test []
  (testing "call->interval"
    (let [event {:startDate "2000-01-01T00:00:00.000Z"
                 :endDate "2000-01-02T00:00:00.000Z"
                 :allDay false}
          all-day {:startDate "2000-01-01T00:00:00.000Z"
                   :endDate "2000-01-02T00:00:00.000Z"
                   :allDay true}]
      (is
       (=
        (interval/event->interval event)
        {:tick/beginning #time/instant "2000-01-01T00:00:00Z", :tick/end #time/instant "2000-01-02T00:00:00Z"}))
      (is
       (=
        (interval/event->interval all-day)
        {:tick/beginning #time/instant "1999-12-31T23:00:00Z", :tick/end #time/instant "2000-01-01T23:00:00Z"})))))

(deftest round-off-by-quarter-test []
  (testing "round-off-by-quarter"
    (let [interval (t.i/new-interval (t/instant "2000-01-01T00:00:00Z")
                                     (t/instant "2000-01-02T00:00:00Z"))]
      (is
       (=
        (-> interval
            (t/>> (t/new-duration 7 :minutes))
            (interval/round-off-by-quarter))
        interval))
      (is
       (=
        (-> interval
            (t/<< (t/new-duration 7 :minutes))
            (interval/round-off-by-quarter))
        interval))
      (is
       (=
        (-> interval
            (t/>> (t/new-duration 8 :minutes))
            (interval/round-off-by-quarter))
        (-> interval
            (t/>> (t/new-duration 15 :minutes)))))
      (is
       (=
        (-> interval
            (t/<< (t/new-duration 8 :minutes))
            (interval/round-off-by-quarter))
        (-> interval
            (t/<< (t/new-duration 15 :minutes))))))))

(deftest round-up-to-half-hour-test []
  (testing "round-up-to-half-hour"
    (let [interval (t.i/new-interval (t/instant "2000-01-01T00:00:00Z")
                                     (t/instant "2000-01-02T00:00:00Z"))]
      (is
       (=
        (-> interval
            (t.i/extend (t/new-duration -1 :minutes))
            (interval/round-up-to-half-hour))
        interval))
      (is
       (=
        (-> interval
            (t.i/extend (t/new-duration 1 :minutes))
            (interval/round-up-to-half-hour))
        (-> interval
            (t.i/extend (t/new-duration 30 :minutes))))))))

(deftest concur?-test []
  (let [twentieth-century (t.i/new-interval (t/instant "1901-01-01T00:00:00Z")
                                            (t/instant "2000-01-01T00:00:00Z"))
        eighties (t.i/new-interval (t/instant "1980-01-01T00:00:00Z")
                                   (t/instant "1990-01-01T00:00:00Z"))
        year-1999 (t.i/new-interval (t/instant "1999-01-01T00:00:00Z")
                                    (t/instant "2000-01-01T00:00:00Z"))
        year-2000 (t.i/new-interval (t/instant "2000-01-01T00:00:00Z")
                                    (t/instant "2001-01-01T00:00:00Z"))]
    (testing "Concur? meeting intervals"
      (is
       (true?
        (interval/concur? twentieth-century year-1999))))
    (testing "Concur? containing intervals"
      (is
       (true?
        (interval/concur? eighties twentieth-century))))
    (testing "Concur? during intervals"
      (is
       (true?
        (interval/concur? twentieth-century eighties))))
    (testing "Concur? met-by intervals"
      (is
       (true?
        (false? (interval/concur? twentieth-century year-2000)))))
    (testing "Concur? preceding intervals"
      (is
       (false?
        (interval/concur? year-2000 eighties))))

    (testing "Concur? preceded-by intervals"
      (is
       (false?
        (interval/concur? eighties year-2000))))
    (testing "Concur? empty intervals"
      (is
       (false?
        (interval/concur? nil year-2000)))
      (is
       (false?
        (interval/concur? year-2000 nil)))
      (is
       (false?
        (interval/concur? nil nil))))))

(deftest union-test
  (testing "Union meeting intervals"
    (is
     (=
      [(t.i/new-interval (t/date "2017-06-15") (t/date "2017-06-25"))
       (t.i/new-interval (t/date "2017-06-26") (t/date "2017-06-30"))]
      (interval/union [(t.i/new-interval (t/date "2017-06-15") (t/date "2017-06-25"))
                       (t.i/new-interval (t/date "2017-06-26") (t/date "2017-06-30"))]))))
  (testing "Union overlapping intervals"
    (is
     (=
      [(t.i/new-interval (t/date "2017-06-10") (t/date "2017-06-25"))
       (t.i/new-interval (t/date "2017-07-01") (t/date "2017-07-10"))]
      (interval/union [(t.i/new-interval (t/date "2017-06-10") (t/date "2017-06-20"))
                       (t.i/new-interval (t/date "2017-06-15") (t/date "2017-06-25"))
                       (t.i/new-interval (t/date "2017-07-01") (t/date "2017-07-10"))]))))
  (testing "Union containing intervals"
    (is
     (=
      [(t.i/new-interval (t/date "2017-06-15") (t/date "2017-06-25"))]
      (interval/union [(t.i/new-interval (t/date "2017-06-15") (t/date "2017-06-25"))
                       (t.i/new-interval (t/date "2017-06-17") (t/date "2017-06-20"))]))))

  (testing "Union finished-by intervals"
    (is
     (=
      [(t.i/new-interval (t/date "2017-06-15") (t/date "2017-06-25"))]
      (interval/union [(t.i/new-interval (t/date "2017-06-15") (t/date "2017-06-25"))
                       (t.i/new-interval (t/date "2017-06-17") (t/date "2017-06-20"))
                       (t.i/new-interval (t/date "2017-06-18") (t/date "2017-06-25"))])))))

(deftest subtract-test
  (testing "Subtract meeting intervals"
    (is
     (=
      (interval/subtract (t.i/new-interval (t/date "2017-06-15") (t/date "2017-06-25"))
                         (t.i/new-interval (t/date "2017-06-26") (t/date "2017-06-30")))
      [(t.i/new-interval (t/date "2017-06-15") (t/date "2017-06-25"))])))
  (testing "Subtract overlapping intervals"
    (is
     (=
      (interval/subtract (t.i/new-interval (t/date "2017-06-10") (t/date "2017-06-20"))
                         (t.i/new-interval (t/date "2017-06-15") (t/date "2017-06-25")))
      [(t.i/new-interval (t/date "2017-06-10") (t/date "2017-06-14"))])))
  (testing "Subtract containing intervals"
    (is
     (=
      (interval/subtract (t.i/new-interval (t/date "2017-06-15") (t/date "2017-06-25"))
                         (t.i/new-interval (t/date "2017-06-17") (t/date "2017-06-20")))
      [(t.i/new-interval (t/date "2017-06-15") (t/date "2017-06-16"))
       (t.i/new-interval (t/date "2017-06-21") (t/date "2017-06-25"))])))
  (testing "Subtract finished-by intervals"
    (is
     (=
      (interval/subtract (t.i/new-interval (t/date "2017-06-15") (t/date "2017-06-25"))
                         (t.i/new-interval (t/date "2017-06-18") (t/date "2017-06-25")))
      [(t.i/new-interval (t/date "2017-06-15") (t/date "2017-06-17"))])))
  (testing "Subtract empty intervals"
    (is
     (=
      (interval/subtract nil
                         (t.i/new-interval (t/date "2017-06-26") (t/date "2017-06-30")))
      []))
    (is
     (=
      (interval/subtract (t.i/new-interval (t/date "2017-06-26") (t/date "2017-06-30"))
                         nil)
      [(t.i/new-interval (t/date "2017-06-26") (t/date "2017-06-30"))]))))

(deftest split-at-midnight-test []
  (testing "split-at-midnight"
    (is
     (=
      (interval/split-at-midnight [(t.i/new-interval (t/instant "2017-06-15T22:00:00Z") (t/instant "2017-06-16T22:00:00Z"))
                                   (t.i/new-interval (t/instant "2017-06-21T06:30:00Z") (t/instant "2017-06-23T15:00:00Z"))])
      [(t.i/new-interval (t/instant "2017-06-15T22:00:00Z") (t/instant "2017-06-16T22:00:00Z"))
       (t.i/new-interval (t/instant "2017-06-21T06:30:00Z") (t/instant "2017-06-21T22:00:00Z"))
       (t.i/new-interval (t/instant "2017-06-21T22:00:00Z") (t/instant "2017-06-22T22:00:00Z"))
       (t.i/new-interval (t/instant "2017-06-22T22:00:00Z") (t/instant "2017-06-23T15:00:00Z"))]))))
