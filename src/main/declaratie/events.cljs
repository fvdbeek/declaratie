(ns declaratie.events
  (:require
   [re-frame.core :as rf]
   [day8.re-frame.async-flow-fx :as async-flow-fx]
   [clojure.set :as set]
   [clojure.string :as str]
   [cognitect.transit :as transit]
   [tick.core :as t]
   [tick.alpha.interval :as t.i]
   ["react-native-call-log" :as call-logs]
   ["react-native-calendar-events" :refer [default] :rename {default RNCalendarEvents}]
   ["@react-native-async-storage/async-storage" :refer [default] :rename {default AsyncStorage}]
   [declaratie.db :as db :refer [app-db]]
   [declaratie.dates :as dates]
   [declaratie.interval :as interval]
   [declaratie.time :as time]))

(def db-key "declaratie:db")

(def calls-key "declaratie:calls")

;; Helpers

;; Async flow (See https://github.com/Day8/re-frame-async-flow-fx)

(defn boot-flow
  []
  {:first-dispatch [:load-localstore]
   :rules [{:when :seen? :events :success-load-localstore :dispatch-n '([:init-current-date] [:load-calendars] [:read-calls])}
           {:when :seen? :events :success-load-calendars :dispatch [:init-events]}
           {:when :seen-both? :events [:success-load-events] :halt? true}
           {:when :seen-any-of? :events [:fail-load-localstore :fail-load-calendars :fail-load-events] :halt? true}]})

;; Interceptors

(def validate-spec
  [(when ^boolean goog.DEBUG (rf/after (partial db/check-and-throw ::db/app-db)))])

(def debug
  [(when ^boolean goog.DEBUG re-frame.core/debug)])

;; Effect Handlers

(rf/reg-fx
 :get-localstore
 (fn [{:keys [key on-success on-failure]}]
   (-> (.getItem AsyncStorage key)
       (.then on-success)
       (.catch on-failure))))

(rf/reg-fx
 :set-localstore
 (fn [{:keys [key value on-success on-failure]}]
   (-> (.setItem AsyncStorage key value)
       (.then on-success)
       (.catch on-failure))))

(rf/reg-fx
 :fetch-calendars
 (fn [{:keys [on-success on-failure]}]
   (when goog.DEBUG (js/console.log "Fetching calendars..."))
   (-> (.findCalendars RNCalendarEvents)
       (.then on-success)
       (.catch on-failure))))

(rf/reg-fx
 :fetch-events
 (fn [{:keys [start end calendars on-success on-failure]}]
   (when goog.DEBUG (js/console.log "Fetching events from" start "to" end "..."))
   (-> (.fetchAllEvents RNCalendarEvents
                        (.toISOString (t/inst start))
                        (.toISOString (t/inst end))
                        (clj->js calendars))
       (.then #(js->clj % :keywordize-keys true))
       (.then #(map (fn [event] (assoc event :interval (interval/event->interval event))) %))
       ;; Remove events with a start time before the requested interval to prevent duplicates
       (.then #(remove (fn [event] (t/< (t/beginning (:interval event)) start)) %))
       (.then #(sort-by (comp t/beginning :interval) t/< %))
       ;; Use the hash as an unique key because of duplicate ids in repeating events
       (.then #(map (fn [event] (assoc event :key (hash event))) %))
       (.then on-success)
       (.catch on-failure))))

(rf/reg-fx
 :fetch-calls
 (fn [{:keys [limit min-timestamp on-success on-failure]}]
   (when goog.DEBUG (js/console.log "Fetching calls from" (time/timestamp->instant min-timestamp) "..."))
   (let [filter #js {:minTimestamp min-timestamp}]
     (-> (call-logs/load limit filter)
         (.then on-success)
         (.catch on-failure)))))

;; Coeffect Handlers

(rf/reg-cofx
 :now
 (fn [cofx _]
   (assoc cofx :now (t/instant))))

(rf/reg-cofx
 :uuid
 (fn [cofx _]
   (let [uuid (str (random-uuid))]
     (assoc cofx :uuid uuid))))

;; Event Handlers

(rf/reg-event-fx
 :boot
 validate-spec
 (fn [_ _]
   {:db app-db
    :async-flow (boot-flow)}))

(rf/reg-event-fx
 :load-localstore
 (fn [{db :db} _]
   (when goog.DEBUG (js/console.log "Loading localstore..."))
   (let [r (transit/reader :json {:handlers dates/readers})]
     {:db (assoc db :loading-localstore true)
      :get-localstore {:key db-key
                       :on-success #(rf/dispatch [:success-load-localstore (transit/read r %)])
                       :on-failure #(rf/dispatch [:fail-load-localstore %])}})))

(rf/reg-event-fx
 :save-localstore
 (fn [cofx _]
   (when goog.DEBUG (js/console.log "Writing localstore..."))
   (let [db (:db cofx)
         value (select-keys db [:db-version
                                :calendars
                                :calendar-ids
                                :activities
                                :selected-events
                                :selected-call-ids
                                :selected-activity-ids
                                :dark-theme
                                :storage-directory
                                :calendar-visible?
                                :stopwatch-visible?
                                :stopwatch-running?
                                :stopwatch-interval
                                :personal-name
                                :personal-number
                                :personal-premium])
         w (transit/writer :json {:handlers dates/writers})]
     {:db (assoc db :loading-localstore true)
      :set-localstore {:key db-key
                       :value (transit/write w value)
                       :on-success #(rf/dispatch [:set-loading-localstore false])
                       :on-failure (fn [error] (rf/dispatch [:set-error-message (.-message error)]))}})))

(rf/reg-event-db
 :success-load-localstore
 validate-spec
 (fn [db [_ value]]
   (when goog.DEBUG (js/console.log "Loading localstore...done"))
   (-> db
       (merge value)
       (assoc :loading-localstore false))))

(rf/reg-event-fx
 :fail-load-localstore
 (fn [cofx [_ error-message]]
   (when goog.DEBUG (js/console.log "Loading localstore...failed:" error-message))
   (let [db (:db cofx)]
     {:db (assoc db :loading-localstore false)
      :fx [[:dispatch [:set-error-message "Loading localstore failed"]]]})))

(rf/reg-event-fx
 :load-calls
 (fn [cofx [_ init?]]
   (when goog.DEBUG (js/console.log "Loading calls..."))
   (let [db (:db cofx)
         timestamp (if init? 0 (inc (:last-sync db)))]
     {:db (assoc db :loading-calls? true)
      :fetch-calls {:limit -1 ;; use -1 for no limit
                    :min-timestamp timestamp
                    :on-success #(rf/dispatch [:success-load-calls (js->clj % :keywordize-keys true) init?])
                    :on-failure #(rf/dispatch [:fail-load-calls %])}})))

(rf/reg-event-fx
 :read-calls
 (fn [_ _]
   (when goog.DEBUG (js/console.log "Reading calls..."))
   (let [r (transit/reader :json {:handlers dates/readers})]
     {:get-localstore {:key calls-key
                       :on-success #(rf/dispatch [:success-load-localstore (transit/read r %)])
                       :on-failure #(rf/dispatch [:fail-load-localstore %])}})))

(rf/reg-event-fx
 :write-calls
 (fn [cofx _]
   (when goog.DEBUG (js/console.log "Writing calls..."))
   (let [db (:db cofx)
         value (select-keys db [:last-sync
                                :calls
                                :calls-limit-count
                                :calls-limit-period])
         w (transit/writer :json {:handlers dates/writers})]
     {:set-localstore {:key calls-key
                       :value (transit/write w value)
                       :on-success #(rf/dispatch [:set-loading-localstore false])
                       :on-failure (fn [error] (rf/dispatch [:set-error-message (.-message error)]))}})))

(rf/reg-event-fx
 :prune-calls
 [(rf/inject-cofx :now)]
 (fn [cofx _]
   (let [db (:db cofx)
         now (:now cofx)
         calls (:calls db)
         ids (:selected-call-ids db)
         limit-count (:calls-limit-count db)
         limit-period (:calls-limit-period db)
         timestamp (->> limit-period
                        (apply t/new-period)
                        (t/<< (t/date-time now))
                        (t/instant)
                        (time/instant->timestamp))
         pruned-calls (->> calls
                           (filter #(> (:id %) timestamp))
                           (take limit-count)
                           (into []))
         pruned-ids (->> pruned-calls
                         (map :id)
                         (remove ids))]
     {:db (-> db
              (assoc :calls pruned-calls)
              (update :selected-call-ids #(apply disj % pruned-ids) ))
      :fx [[:dispatch [:write-calls]]]})))

(rf/reg-event-fx
 :success-load-calls
 [(rf/inject-cofx :now)]
 (fn [cofx [_ calls init?]]
   (when goog.DEBUG (js/console.log "Loading calls...done"))
   (let [db (:db cofx)
         now (:now cofx)
         beg (t/epoch)
         duration (t/duration {:tick/beginning beg
                               :tick/end now})
         ;; number of milliseconds since 1970/01/01
         timestamp (t/millis duration)
         new-calls (->> calls
                        (map #(assoc % :interval (interval/call->interval %)))
                        (map #(assoc % :id (js/parseInt (:timestamp %))))
                        (into []))]
     {:db
      (if init?
        (-> db
            (assoc :calls new-calls)
            (assoc :last-sync timestamp)
            (assoc :loading-calls? false))
        (-> db
            (update :calls #(into [] (concat new-calls %)))
            (assoc :last-sync timestamp)
            (assoc :loading-calls? false)))
      :fx [[:dispatch [:prune-calls]]]})))

(rf/reg-event-fx
 :fail-load-calls
 (fn [cofx [_ error-message]]
   (when goog.DEBUG (js/console.log "Loading calls...failed:" error-message))
   (let [db (:db cofx)]
     {:db (assoc db :loading-calls? false)
      :fx [[:dispatch [:set-error-message "Loading calls failed"]]]})))

(rf/reg-event-fx
 :load-calendars
 (fn [cofx _]
   (when goog.DEBUG (js/console.log "Loading calendars..."))
   (let [db (:db cofx)]
     {:db (assoc db :loading-calendars? true)
      :fetch-calendars {:on-success #(rf/dispatch [:success-load-calendars (js->clj % :keywordize-keys true)])
                        :on-failure #(rf/dispatch [:fail-load-calendars %])}})))

(rf/reg-event-fx
 :fail-load-calendars
 (fn [cofx [_ error-message]]
   (when goog.DEBUG (js/console.log "Loading calendars...failed:" error-message))
   (let [db (:db cofx)]
     {:db (assoc db :loading-calendars? false)
      :fx [[:dispatch [:set-error-message "Loading calendars failed"]]]})))

(rf/reg-event-fx
 :success-load-calendars
 (fn [cofx [_ calendars]]
   (when goog.DEBUG (js/console.log "Loading calendars...done"))
   (let [db (:db cofx)
         ids (->> calendars
                  (map :id)
                  (into #{}))]
     {:db (-> db
              (assoc :calendars calendars)
              (update :calendar-ids #(set/intersection % ids))
              (assoc :loading-calendars? false))
      :fx [[:dispatch [:save-localstore]]]})))

(rf/reg-event-fx
 :toggle-calendar-id
 (fn [cofx [_ id]]
   (let [db (:db cofx)]
     {:db (update db :calendar-ids #(if (contains? %1 %2) (disj %1 %2) (conj %1 %2)) id)
      :fx [[:dispatch [:save-localstore]]
           [:dispatch [:load-events]]]})))

(rf/reg-event-fx
 :set-calendar-bounds
 (fn [cofx [_ bounds]]
   (let [db (:db cofx)]
     {:db (assoc db :calendar-bounds bounds)
      :fx [[:dispatch [:load-events]]]})))

(rf/reg-event-fx
 :extend-calendar-bounds
 (fn [cofx [_ number units]]
   (let [db (:db cofx)
         current-bounds (:calendar-bounds db)
         period (t/new-period number units)
         add (cond (pos? number) :after
                   (neg? number) :before)
         extended-bounds (case add
                           :before (-> current-bounds
                                       t/beginning
                                       (t.i/extend (t/new-period (- number) units))
                                       (t/<< (t/new-period (- number) units)))
                           :after (-> current-bounds
                                      t/end
                                      (t.i/extend (t/new-period number units))))
         new-bounds (case add
                      :before (t.i/new-interval (t/beginning extended-bounds) (t/end current-bounds))
                      :after (t.i/new-interval (t/beginning current-bounds) (t/end extended-bounds)))]
     {:db (assoc db :calendar-bounds new-bounds)
      :fx [[:dispatch [:add-events extended-bounds add]]]})))

(rf/reg-event-fx
 :init-events
 [(rf/inject-cofx :now)]
 (fn [cofx _]
   (when goog.DEBUG (js/console.log "Initializing events..."))
   (let [db (:db cofx)
         now (:now cofx)
         calendars (:calendar-ids db)
         bounds (-> now
                    (t/year-month)
                    (t.i/bounds))
         start (t/beginning bounds)
         end (t/end bounds)]
     {:db (assoc db :calendar-bounds bounds)
      :fx [[:dispatch [:event-dispatcher {:start start
                                          :end end
                                          :calendars calendars}]]]})))

(rf/reg-event-fx
 :load-events
 (fn [cofx _]
   (when goog.DEBUG (js/console.log "Loading events..."))
   (let [db (:db cofx)
         loading? (:loading-events? db)]
     (when-not loading?
       (let [bounds (:calendar-bounds db)
             start (t/beginning bounds)
             end (t/end bounds)
             calendars (:calendar-ids db)]
         {:db (assoc db :loading-events? true)
          :fx [[:dispatch [:event-dispatcher {:start start
                                              :end end
                                              :calendars calendars}]]]})))))

(rf/reg-event-fx
 :add-events
 (fn [cofx [_ bounds add]]
   (when goog.DEBUG (js/console.log "Adding events..."))
   (let [db (:db cofx)
         loading? (:loading-events? db)]
     (when-not loading?
       (let [calendars (:calendar-ids db)
             start (t/beginning bounds)
             end (t/end bounds)]
         {:db (assoc db :loading-localstore true)
          :fx [[:dispatch [:event-dispatcher {:start start
                                              :end end
                                              :calendars calendars
                                              :add add}]]]})))))

(rf/reg-event-fx
 :event-dispatcher
 (fn [cofx [_ args]]
   (let [db (:db cofx)
         {:keys [start end calendars add result]} args
         done? (t/>= start end)]
     (if done?
       (case add
         :before {:fx [[:dispatch [:prepend-events result]]]}
         :after {:fx [[:dispatch [:append-events result]]]}
         {:fx [[:dispatch [:success-load-events result]]]})
       (let [next (t/>> start (t/new-period 7 :days))]
         {:fetch-events {:start start
                         :end (t/min next end)
                         :calendars calendars
                         :on-success #(rf/dispatch [:event-dispatcher {:start next
                                                                       :end end
                                                                       :calendars calendars
                                                                       :add add
                                                                       :result (into [] (concat result %))}])
                         :on-failure #(rf/dispatch [:fail-load-events %])}})))))

(rf/reg-event-fx
 :prepend-events
 (fn [cofx [_ result]]
   (when goog.DEBUG (js/console.log "Prepending events..."))
   (let [db (:db cofx)
         events (:events db)]
     {:fx [[:dispatch [:success-load-events (into [] (concat result events))]]]})))

(rf/reg-event-fx
 :append-events
 (fn [cofx [_ result]]
   (when goog.DEBUG (js/console.log "Appending events..."))
   (let [db (:db cofx)
         events (:events db)]
     {:fx [[:dispatch [:success-load-events (into [] (concat events result))]]]})))

(rf/reg-event-db
 :success-load-events
 validate-spec
 (fn [db [_ events]]
   (when goog.DEBUG (js/console.log "Loading events...done"))
   (-> db
       (assoc :events events)
       (assoc :loading-events? false))))

(rf/reg-event-fx
 :fail-load-events
 (fn [cofx [_ error-message]]
   (when goog.DEBUG (js/console.log "Loading events...failed:" error-message))
   (let [db (:db cofx)]
     {:db (assoc db :loading-events? false)
      :fx [[:dispatch [:set-error-message "Loading events failed"]]]})))

(rf/reg-event-fx
 :toggle-event
 (fn [cofx [_ event]]
   (let [db (:db cofx)
         events (:selected-events db)
         key (:key event)
         selected? (->> events
                        (map :key)
                        (some #{key})
                        some?)]
     (if selected?
       {:db (update db :selected-events disj event)
        :fx [[:dispatch [:deselect-calls event]]
             [:dispatch [:deselect-activities event]]
             [:dispatch [:save-localstore]]]}
       {:db (update db :selected-events conj event)
        :fx [[:dispatch [:select-calls event]]
             [:dispatch [:select-activities event]]
             [:dispatch [:save-localstore]]]}))))

(rf/reg-event-fx
 :select-event
 (fn [cofx [_ event]]
   (let [db (:db cofx)]
     {:db (update db :selected-events conj event)
      :fx [[:dispatch [:select-calls event]]
           [:dispatch [:select-activities event]]
           [:dispatch [:save-localstore]]]})))

(rf/reg-event-fx
 :clear-events
 (fn [cofx _]
   (let [db (:db cofx)]
     {:db (-> db
              (assoc :selected-events #{})
              (assoc :selected-call-ids #{})
              (assoc :selected-activity-ids #{}))
      :fx [[:dispatch [:save-localstore]]]})))

(rf/reg-event-fx
 :select-calls
 (fn [cofx [_ event]]
   (let [db (:db cofx)
         calls (:calls db)
         during-event? (fn [call]
                         (some-> call
                                 :interval
                                 t/beginning
                                 (interval/concur? (:interval event))))
         ids (->> calls
                  (filter during-event?)
                  (map :id))]
     {:db (update db :selected-call-ids into ids)})))

(rf/reg-event-fx
 :deselect-calls
 (fn [cofx [_ event]]
   (let [db (:db cofx)
         events (:selected-events db)
         calls (:calls db)
         selected-ids (:selected-call-ids db)
         selected-calls (filter #(contains? selected-ids (:id %)) calls)
         intervals (apply interval/subtract (:interval event) (map :interval events))
         during? (fn [instant intervals]
                   (->> intervals
                        (some #(interval/concur? instant %))))
         ids (->> selected-calls
                  (filter (fn [call] (some-> call
                                             :interval
                                             t/beginning
                                             (during? intervals))))
                  (map :id))]
     {:db (update db :selected-call-ids #(apply disj % ids))})))

(rf/reg-event-fx
 :toggle-call
 (fn [cofx [_ id]]
   (let [db (:db cofx)
         ids (:selected-call-ids db)
         selected? (contains? ids id)]
     (if selected?
       {:db (update db :selected-call-ids disj id)
        :fx [[:dispatch [:save-localstore]]]}
       {:db (update db :selected-call-ids conj id)
        :fx [[:dispatch [:save-localstore]]]}))))

(rf/reg-event-fx
 :select-activities
 (fn [cofx [_ event]]
   (let [db (:db cofx)
         activities (:activities db)
         during-event? (fn [activity]
                         (-> activity
                             :interval
                             t/beginning
                             (interval/concur? (:interval event))))
         ids (->> activities
                  (filter (fn [[_ activity]] during-event?))
                  (map key))]
     {:db (update db :selected-activity-ids #(apply conj % ids))})))

(rf/reg-event-fx
 :deselect-activities
 (fn [cofx [_ event]]
   (let [db (:db cofx)
         events (:selected-events db)
         activities (:activities db)
         selected-ids (:selected-activity-ids db)
         intervals (apply interval/subtract (:interval event) (map :interval events))
         during? (fn [instant intervals]
                   (->> intervals
                        (some #(interval/concur? instant %))))
         ids (->> selected-ids
                  (select-keys activities)
                  (filter (fn [[_ activity]] (-> activity
                                                 :interval
                                                 t/beginning
                                                 (during? intervals))))
                  keys)]
     {:db (update db :selected-activity-ids #(apply disj % ids))})))

(rf/reg-event-fx
 :toggle-activity
 (fn [cofx [_ id]]
   (let [db (:db cofx)
         activities (:selected-activity-ids db)
         selected? (contains? activities id)]
     (if selected?
       {:db (update db :selected-activity-ids disj id)
        :fx [[:dispatch [:save-localstore]]]}
       {:db (update db :selected-activity-ids conj id)
        :fx [[:dispatch [:save-localstore]]]}))))

(rf/reg-event-db
 :set-query
 validate-spec
 (fn [db [_ query]]
   (assoc db :query query)))

(rf/reg-event-fx
 :init-current-date
 [(rf/inject-cofx :now)]
 (fn [cofx _]
   (when goog.DEBUG (js/console.log "Initializing current date..."))
   (let [db (:db cofx)
         now (:now cofx)
         today (t/date now)]
     {:db (-> db
              (assoc :current-date today))})))

(rf/reg-event-db
 :set-current-date
 validate-spec
 (fn [db [_ date]]
   (assoc db :current-date date)))

(rf/reg-event-db
 :update-current-date
 validate-spec
 (fn [db [_ number units]]
   (update db :current-date #(t/>> % (t/new-period number units)))))

(rf/reg-event-fx
 :toggle-theme
 (fn [cofx _]
   (let [db (:db cofx)]
     {:db (update db :dark-theme not)
      :fx [[:dispatch [:save-localstore]]]})))

(rf/reg-event-fx
 :set-storage-directory
 (fn [cofx [_ directory]]
   (let [db (:db cofx)]
     {:db (assoc db :storage-directory directory)
      :fx [[:dispatch [:save-localstore]]]})))

(rf/reg-event-fx
 :add-activity
 [(rf/inject-cofx :uuid)]
 (fn [cofx [_ interval]]
   (let [db (:db cofx)
         id (:uuid cofx)
         activity {:interval interval}]
     {:db (assoc-in db [:activities id] activity)
      :fx [[:dispatch [:save-localstore]]
           [:dispatch [:maybe-select-activity id]]]})))

(rf/reg-event-fx
 :edit-activity
 (fn [cofx [_ id interval]]
   (let [db (:db cofx)
         activity {:interval interval}]
     {:db (assoc-in db [:activities id] activity)
      :fx [[:dispatch [:save-localstore]]
           [:dispatch [:maybe-select-activity id]]]})))

(rf/reg-event-fx
 :remove-activity
 (fn [cofx [_ id]]
   (let [db (:db cofx)]
     {:db (-> db
              (update :activities dissoc id)
              (update :selected-activity-ids disj id))
      :fx [[:dispatch [:save-localstore]]]})))

(rf/reg-event-fx
 :maybe-select-activity
 (fn [cofx [_ id]]
   (let [db (:db cofx)
         events (:selected-events db)
         activity (get-in db [:activities id])
         during? (fn [instant intervals]
                   (->> intervals
                        (some #(interval/concur? instant %))))
         selected? (->> events
                        (map :interval)
                        (filter some?)
                        (during? (-> activity
                                     :interval
                                     t/beginning)))]
     {:db (update db :selected-activity-ids (if selected? conj disj) id)
      :fx [[:dispatch [:save-localstore]]]})))

(rf/reg-event-fx
 :toggle-stopwatch
 (fn [cofx _]
   (let [db (:db cofx)]
     {:db (update db :stopwatch-running? not)
      :fx [[:dispatch [:save-localstore]]]})))

(rf/reg-event-fx
 :init-stopwatch
 [(rf/inject-cofx :now)]
 (fn [cofx _]
   (let [db (:db cofx)
         now (:now cofx)]
     {:db (assoc-in db [:stopwatch-interval :start] now)
      :fx [[:dispatch [:save-localstore]]]})))

(rf/reg-event-fx
 :update-stopwatch
 [(rf/inject-cofx :now)]
 (fn [cofx _]
   (let [db (:db cofx)
         now (:now cofx)]
     {:db (assoc-in db [:stopwatch-interval :end] now)})))

(rf/reg-event-fx
 :reset-stopwatch
 (fn [cofx _]
   (let [db (:db cofx)]
     {:db (-> db
              (assoc :stopwatch-running? false)
              (assoc :stopwatch-interval {:start nil
                                          :end nil}))
      :fx [[:dispatch [:save-localstore]]]})))

(rf/reg-event-db
 :set-event-filter
 validate-spec
 (fn [db [_ event-filter]]
   (assoc db :event-filter event-filter)))

(rf/reg-event-db
 :set-call-filter
 validate-spec
 (fn [db [_ call-filter]]
   (assoc db :call-filter call-filter)))

(rf/reg-event-db
 :set-activity-filter
 validate-spec
 (fn [db [_ activity-filter]]
   (assoc db :activity-filter activity-filter)))

(rf/reg-event-db
 :set-calendar-visible
 validate-spec
 (fn [db [_ visible?]]
   (assoc db :calendar-visible? (if visible? true false))))

(rf/reg-event-db
 :toggle-calendar-visible
 validate-spec
 (fn [db _]
   (update db :calendar-visible? not)))

(rf/reg-event-db
 :set-stopwatch-visible
 validate-spec
 (fn [db [_ visible?]]
   (-> db
       (assoc :stopwatch-visible? (if visible? true false)))))

(rf/reg-event-db
 :toggle-stopwatch-visible
 validate-spec
 (fn [db _]
   (update db :stopwatch-visible? not)))

(rf/reg-event-fx
 :set-personal-name
 (fn [cofx [_ name]]
   (let [db (:db cofx)]
     {:db (assoc db :personal-name name)
      :fx [[:dispatch [:save-localstore]]]})))

(rf/reg-event-fx
 :set-personal-number
 (fn [cofx [_ number]]
   (let [db (:db cofx)]
     {:db (assoc db :personal-number number)
      :fx [[:dispatch [:save-localstore]]]})))

(rf/reg-event-fx
 :set-personal-premium
 (fn [cofx [_ arrangement]]
   (let [db (:db cofx)]
     {:db (assoc db :personal-premium arrangement)
      :fx [[:dispatch [:save-localstore]]]})))

(rf/reg-event-fx
 :set-calls-limit-count
 (fn [cofx [_ n]]
   (let [db (:db cofx)]
     {:db (assoc db :calls-limit-count n)
      :fx [[:dispatch [:save-localstore]]]})))

(rf/reg-event-fx
 :set-calls-limit-period
 (fn [cofx [_ n u]]
   (let [db (:db cofx)]
     {:db (assoc db :calls-limit-period [n u])
      :fx [[:dispatch [:save-localstore]]]})))

(rf/reg-event-db
 :set-loading-localstore
 validate-spec
 (fn [db [_ loading?]]
   (assoc db :loading-localstore loading?)))

(rf/reg-event-db
 :set-loading-calendars
 validate-spec
 (fn [db [_ loading?]]
   (assoc db :loading-calendars? loading?)))

(rf/reg-event-db
 :set-loading-events
 validate-spec
 (fn [db [_ loading?]]
   (assoc db :loading-events? loading?)))

(rf/reg-event-db
 :set-loading-calls
 validate-spec
 (fn [db [_ loading?]]
   (assoc db :loading-calls? loading?)))

(rf/reg-event-db
 :set-loading-table
 validate-spec
 (fn [db [_ loading?]]
   (assoc db :loading-table? loading?)))

(rf/reg-event-db
 :set-error-message
 validate-spec
 (fn [db [_ error]]
   (assoc db :error-message error)))
