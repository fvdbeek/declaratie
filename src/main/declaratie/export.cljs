(ns declaratie.export
  (:require [re-frame.core :as rf]
            [tick.core :as t]
            [tick.alpha.interval :as t.i]
            ["react-native-file-access" :as rnfs]
            ["exceljs" :as exceljs]
            ["rfc4648" :refer [base64]]
            [declaratie.holiday :as holiday]
            [declaratie.time :as time]))

(def cache-directory
  "The absolute path to the caches directory. The files are removed on app uninstall."
  (.-CacheDir rnfs/Dirs))

(def asset-path
  "Relative path of the xlsx template from the root of the assets folder."
  "xlsx/declaratie.xlsx")

(def worksheet-name
  "Name of the worksheet"
  "Declaratie Crisisdienst")

(def row-start
  "Start row of the table."
  20)

(def table-ref
  "Cell of the table."
  "B20")

(def name-ref
  "Cell of the name."
  "C3")

(def number-ref
  "Cell of the number."
  "C4")

(def premium-pay-ref
  "Cell of the premium pay choice."
  "H12")

(def premium-time-ref
  "Cell of the premium time choice."
  "H13")

(defn load-xlsx []
  "Load workbook from xlsx."
  (let [workbook (new exceljs/Workbook)
        basename (.basename rnfs/Util asset-path)
        cache-path (str cache-directory "/" basename)]
    ;; copy asset to cache
    (-> (.cpAsset rnfs/FileSystem asset-path cache-path "asset")
        ;; read xslx from cache
        (.then #(.readFile rnfs/FileSystem cache-path "base64"))
        ;; parse base64 string to buffer
        (.then (fn [contents] (.parse base64 contents)))
        ;; load workbook from buffer
        (.then (fn [buffer] (-> workbook
                                (.-xlsx)
                                (.load buffer)))))))

(defn interval->row
  "Populate a row from an interval."
  [interval]
  (let [standby? (some? (:standby interval))
        worked? (some? (:worked interval))
        start (t/beginning (or (:standby interval)
                               (:worked interval)))
        end (t/end (or (:standby interval)
                       (:worked interval)))
        untill-midnight? (not= (t/date start) (t/date end))
        weekday-number (-> start
                           t/date
                           t/day-of-week
                           t/int)
        holiday? (holiday/holiday? (t/date start))
        weekdays? (and (not holiday?)
                       (some #(= weekday-number %) [1 2 3 4 5]))
        weekend? (and (not holiday?)
                      (some #(= weekday-number %) [0 6]))
        date (t/inst (time/instant->utc start))
        weekday (time/format-date start "EEEE")
        start-hour-standby (when standby? (t/hour start))
        start-minute-standby (when standby? (t/minute start))
        end-hour-standby (when standby? (if untill-midnight? 24 (t/hour end)))
        end-minute-standby (when standby? (t/minute end))
        total-hours-standby (when standby?
                              (-> (t.i/new-interval start end)
                                  (t/duration)
                                  (t/divide (t/new-duration 1 :hours))))
        weekdays-standby (when (and standby? weekdays?) total-hours-standby)
        weekend-standby (when (and standby? weekend?) total-hours-standby)
        holiday-standby (when (and standby? holiday?) total-hours-standby)
        start-hour-worked (when worked? (t/hour start))
        start-minute-worked (when worked? (t/minute start))
        end-hour-worked (when worked? (t/hour end))
        end-minute-worked (when worked? (t/minute end))
        daytime (t.i/new-interval (-> start
                                      (t/date)
                                      (t/at (t/time "06:00"))
                                      (t/instant))
                                  (-> start
                                      (t/date)
                                      (t/at (t/time "22:00"))
                                      (t/instant)))
        hours-total (when worked?
                      (-> (t.i/new-interval start end)
                          (t/duration)
                          (t/divide (t/new-duration 1 :hours))))
        hours-daytime (when worked?
                        (some-> (t.i/new-interval start end)
                                (t.i/concur daytime)
                                (t/duration)
                                (t/divide (t/new-duration 1 :hours))))
        premium-50 (or hours-daytime 0)
        premium-100 (- hours-total premium-50)]
    [date
     weekday
     (if holiday? 1 nil)
     start-hour-standby
     start-minute-standby
     end-hour-standby
     end-minute-standby
     total-hours-standby
     weekdays-standby
     weekend-standby
     holiday-standby
     start-hour-worked
     start-minute-worked
     end-hour-worked
     end-minute-worked
     premium-50
     premium-100]))

(defn intervals->xlsx
  [intervals]
  "Converts intervals to an Excel workbook.
Loads an Excel template, then fills it with the personal data and calculated
rows, and finally returns a javascript promise."
  (let [name (rf/subscribe [:get-personal-name])
        number (rf/subscribe [:get-personal-number])
        premium (rf/subscribe [:get-personal-premium])
        rows (->> intervals
                  (map interval->row)
                  (into []))]
    (-> (load-xlsx)
        (.then (fn [workbook]
                 (let [worksheet (.getWorksheet ^js workbook worksheet-name)
                       columns [{:name "date"
                                 :filterButton false
                                 :totalsRowLabel ""
                                 :totalsRowFunction "none"}
                                {:name "weekday"
                                 :filterButton false
                                 :totalsRowLabel ""
                                 :totalsRowFunction "none"}
                                {:name "holiday"
                                 :filterButton false
                                 :totalsRowLabel ""
                                 :totalsRowFunction "none"}
                                {:name "start-hour-standby"
                                 :filterButton false
                                 :totalsRowLabel ""
                                 :totalsRowFunction "none"}
                                {:name "start-minute-standby"
                                 :filterButton false
                                 :totalsRowLabel ""
                                 :totalsRowFunction "none"}
                                {:name "end-hour-standby"
                                 :filterButton false
                                 :totalsRowLabel ""
                                 :totalsRowFunction "none"}
                                {:name "end-minute-standby"
                                 :filterButton false
                                 :totalsRowLabel ""
                                 :totalsRowFunction "none"}
                                {:name "total-hours-standby"
                                 :filterButton false
                                 :totalsRowLabel ""
                                 :totalsRowFunction "none"}
                                {:name "weekdays-standby"
                                 :filterButton false
                                 :totalsRowLabel ""
                                 :totalsRowFunction "none"}
                                {:name "weekend-standby"
                                 :filterButton false
                                 :totalsRowLabel ""
                                 :totalsRowFunction "none"}
                                {:name "holiday-standby"
                                 :filterButton false
                                 :totalsRowLabel ""
                                 :totalsRowFunction "none"}
                                {:name "start-hour-worked"
                                 :filterButton false
                                 :totalsRowLabel ""
                                 :totalsRowFunction "none"}
                                {:name "start-minute-worked"
                                 :filterButton false
                                 :totalsRowLabel ""
                                 :totalsRowFunction "none"}
                                {:name "end-hour-worked"
                                 :filterButton false
                                 :totalsRowLabel ""
                                 :totalsRowFunction "none"}
                                {:name "end-minute-worked"
                                 :filterButton false
                                 :totalsRowLabel ""
                                 :totalsRowFunction "none"}
                                {:name "premium-50"
                                 :filterButton false
                                 :totalsRowLabel ""
                                 :totalsRowFunction "sum"}
                                {:name "premium-100"
                                 :filterButton false
                                 :totalsRowLabel ""
                                 :totalsRowFunction "sum"}]
                       table (clj->js {:name "table"
                                       :ref table-ref
                                       :headerRow false
                                       :totalsRow true
                                       :columns columns
                                       :rows rows})
                       name-cell (.getCell ^js worksheet name-ref)
                       number-cell (.getCell ^js worksheet number-ref)
                       premium-pay-cell (.getCell ^js worksheet premium-pay-ref)
                       premium-time-cell (.getCell ^js worksheet premium-time-ref)
                       row-count (count rows)
                       row-insert true]
                   (set! (.-value name-cell) @name)
                   (set! (.-value number-cell) @number)
                   (case @premium
                     "pay" (do
                             (set! (.-value premium-pay-cell) "Ja")
                             (set! (.-value premium-time-cell) "Nee"))
                     "time" (do
                              (set! (.-value premium-pay-cell) "Nee")
                              (set! (.-value premium-time-cell) "Ja")))
                   (.duplicateRow ^js worksheet row-start (dec row-count) row-insert)
                   (.addTable ^js worksheet table)
                   (.writeBuffer (.-xlsx ^js workbook)))))
        (.then (fn [buffer] (.stringify base64 buffer))))))
