(ns declaratie.edit
  (:require
   [goog.object :as gobj]
   [reagent.core :as r]
   [re-frame.core :as rf]
   [tick.core :as t]
   [tick.alpha.interval :as t.i]
   ["react-native" :as rn]
   ["react-native-paper" :as rnp]
   ["react-native-vector-icons/MaterialCommunityIcons" :refer [default] :rename {default MaterialCommunityIcons}]
   ["react-native-safe-area-context" :refer [SafeAreaView]]
   ["@react-navigation/native" :refer [DrawerActions]]
   ["@react-native-community/datetimepicker" :refer [default] :rename {default DateTimePicker}]
   [declaratie.ref :as ref]
   [declaratie.styles :as styles]
   [declaratie.time :as time]))

(def start (r/atom (-> (t/instant)
                       time/instant->timestamp
                       (time/round-off-by 30)
                       time/timestamp->instant)))

(def end (r/atom (-> @start
                     (t/>> (t/new-duration 30 :minutes)))))

(defn edit-activity [key]
  (let [dark-theme? (rf/subscribe [:dark-theme?])
        activity (rf/subscribe [:get-activity key])
        start (r/atom (or (t/beginning @activity)
                          (-> (t/instant)
                              time/instant->timestamp
                              (time/round-off-by 30)
                              time/timestamp->instant)))
        end (r/atom (or (t/end @activity)
                        (-> @start
                            (t/>> (t/new-duration 30 :minutes)))))
        start-date-visible? (r/atom false)
        start-time-visible? (r/atom false)
        end-date-visible? (r/atom false)
        end-time-visible? (r/atom false)]
    (fn []
      [:> rnp/Dialog
       {:visible true
        :on-dismiss #((:goBack @ref/navigation))}
       [:> (.-Title rnp/Dialog) (if @activity
                                  "Activiteit wijzigen"
                                  "Activiteit toevoegen")]
       [:> (.-Content rnp/Dialog)
        [:> rn/View
         {:flex-direction "row"}
         [:> rn/View
          {:align-items "center"
           :justify-content "center"
           :overflow "hidden"
           :margin 6}
          [:> MaterialCommunityIcons
           (if (t/>= @start @end)
             {:name "alert-circle"
              :size 24
              :color (get-in (if @dark-theme? styles/dark-theme styles/default-theme) [:colors :error])}
             {:name "clock-start"
              :size 24
              :color (get-in (if @dark-theme? styles/dark-theme styles/default-theme) [:colors :placeholder])})]]
         [:> rn/View
          {:flex 1
           :flex-direction "row"
           :justify-content "space-between"}
          [:> rnp/Button
           {:mode "text"
            :uppercase false
            :color (get-in (if @dark-theme? styles/dark-theme styles/default-theme) [:colors :text])
            :on-press #(swap! start-date-visible? not)}
           (time/format-date @start "EEE d MMM yyyy")]
          [:> rnp/Button
           {:mode "text"
            :uppercase false
            :color (get-in (if @dark-theme? styles/dark-theme styles/default-theme) [:colors :text])
            :on-press #(swap! start-time-visible? not)}
           (time/format-date @start "HH:mm")]]]
        [:> rn/View
         {:flex-direction "row"}
         [:> rn/View
          {:align-items "center"
           :justify-content "center"
           :overflow "hidden"
           :margin 6}
          [:> MaterialCommunityIcons
           {:name "clock-end"
            :color (get-in (if @dark-theme? styles/dark-theme styles/default-theme) [:colors :placeholder])
            :size 24}]]
         [:> rn/View
          {:flex 1
           :flex-direction "row"
           :justify-content "space-between"}
          [:> rnp/Button
           {:mode "text"
            :uppercase false
            :color (get-in (if @dark-theme? styles/dark-theme styles/default-theme) [:colors :text])
            :on-press #(swap! end-date-visible? not)}
           (time/format-date @end "EEE d MMM yyyy")]
          [:> rnp/Button
           {:mode "text"
            :uppercase false
            :color (get-in (if @dark-theme? styles/dark-theme styles/default-theme) [:colors :text])
            :on-press #(swap! end-time-visible? not)}
           (time/format-date @end "HH:mm")]]]
        (when @start-date-visible?
          [:> DateTimePicker
           {:value (t/inst @start)
            :mode "date"
            :on-change (fn [event date]
                         (when date
                           (reset! start (t/instant date))
                           (reset! end (-> date
                                           t/instant
                                           (t/>> (t/new-duration 30 :minutes)))))
                         (reset! start-date-visible? false))}])
        (when @start-time-visible?
          [:> DateTimePicker
           {:value (t/inst @start)
            :mode "time"
            :minute-interval 5
            :on-change (fn [event date]
                         (when date
                           (reset! start (t/instant date))
                           (reset! end (-> date
                                           t/instant
                                           (t/>> (t/new-duration 30 :minutes)))))
                         (reset! start-time-visible? false))}])
        (when @end-date-visible?
          [:> DateTimePicker
           {:value (t/inst @end)
            :mode "date"
            :on-change (fn [event date]
                         (when date (reset! end (t/instant date)))
                         (reset! end-date-visible? false))}])
        (when @end-time-visible?
          [:> DateTimePicker
           {:value (t/inst @end)
            :mode "time"
            :minute-interval 5
            :on-change (fn [event date]
                         (when date (reset! end (t/instant date)))
                         (reset! end-time-visible? false))}])]
       [:> (.-Actions rnp/Dialog)
        [:> rnp/Button
         {:on-press #((:goBack @ref/navigation))}
         "Annuleren"]
        [:> rnp/Button
         {:disabled (if (t/>= @start @end) true false)
          :on-press (fn []
                      (if key
                        (rf/dispatch [:edit-activity key (t.i/new-interval @start @end)])
                        (rf/dispatch [:add-activity (t.i/new-interval @start @end)]))
                      ((:goBack @ref/navigation)))}
         "Opslaan"]]])))

(defn screen [props]
  (let [dark-theme? (rf/subscribe [:dark-theme?])]
    (fn [props]
      (let [key (gobj/getValueByKeys props #js ["route" "params" "key"])]
        [:> SafeAreaView
         {:style {:flex 1}}
         [:> rn/StatusBar
          {:background-color (get-in (if @dark-theme? styles/dark-theme styles/default-theme) [:colors :primary])
           :status-bar-style (if @dark-theme? "dark-content" "light-content")}]
         [edit-activity key]]))))
