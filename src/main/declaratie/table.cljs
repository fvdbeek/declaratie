(ns declaratie.table
  (:require
   [reagent.core :as r]
   [re-frame.core :as rf]
   [tick.core :as t]
   ["react-native" :as rn]
   ["react-native-paper" :as rnp]
   ["react-native-vector-icons/MaterialCommunityIcons" :refer [default] :rename {default MaterialCommunityIcons}]
   ["react-native-safe-area-context" :refer [SafeAreaView]]
   ["@react-navigation/native" :refer [DrawerActions]]
   ["react-native-table-component" :refer [Table TableWrapper Row Cell]]
   ["react-native-file-access" :refer [Dirs FileSystem]]
   ["react-native-document-picker" :refer [default] :rename {default RNDocumentPicker}]
   ["react-native-share" :refer [default] :rename {default Share}]
   [declaratie.export :as export]
   [declaratie.holiday :as holiday]
   [declaratie.ref :as ref]
   [declaratie.stopwatch :as stopwatch]
   [declaratie.styles :as styles]
   [declaratie.time :as time]))

(def message (r/atom nil))

(def dialog-visible? (r/atom false))

(def cache-directory
  "The absolute path to the caches directory. The files are removed on app uninstall."
  (.-CacheDir Dirs))

(def internal-directory
  "The absolute path to the app-specific document directory on internal storage. The files are removed on app uninstall."
  (.-DocumentDir Dirs))

(defn unique-filename
  "Return a unique filename.
  If `files` contains `file` a number between parentheses is appended.
  `files` should be a javascript array."
  [files file]
  (if (.includes files file)
    (let [match (re-matches #"(.*)(\.[^.]+)" file)
          [_ base extension] match]
      (loop [number 1]
        (let [new-file (str base "_(" number ")" extension)]
          (if (.includes files new-file)
            (recur (inc number))
            ;; return file with "_(<number)" appended
            new-file))))
    ;; `files` does't contain `file`
    file))

(defn write-file
  [storage file contents encoding]
  ;; First, read the directory to check if the intended filename already exists to prevent overwriting
  (rf/dispatch [:set-loading-table true])
  (case storage
    "internal" (-> (.ls FileSystem internal-directory)
                   (.then (fn [files] (unique-filename files file)))
                   (.then (fn [filename] (.writeFile FileSystem (str internal-directory "/" filename) contents encoding)))
                   (.then #(reset! message "Bestand opgeslagen"))
                   (.then #(rf/dispatch [:set-loading-table false]))
                   (.catch (fn [error]
                             (rf/dispatch [:set-error-message (.-message error)])
                             (rf/dispatch [:set-loading-table false]))))
    "downloads" (-> (.ls FileSystem cache-directory)
                   (.then (fn [files] (unique-filename files file)))
                   (.then (fn [filename] (-> (.writeFile FileSystem (str cache-directory "/" filename) contents encoding)
                                             (.then (fn [] (.cpExternal FileSystem (str cache-directory "/" filename) filename "downloads"))))))
                   (.then #(reset! message "Bestand opgeslagen"))
                   (.then #(rf/dispatch [:set-loading-table false]))
                   (.catch (fn [error]
                             (rf/dispatch [:set-error-message (.-message error)])
                             (rf/dispatch [:set-loading-table false]))))
    (-> (.ls FileSystem storage)
        (.then (fn [files] (unique-filename files file)))
        (.then (fn [filename] (.writeFile FileSystem (str storage "/" filename) contents encoding)))
        (.then #(reset! message "Bestand opgeslagen"))
        (.then #(rf/dispatch [:set-loading-table false]))
        (.catch (fn [error]
                  (rf/dispatch [:set-error-message (.-message error)])
                  (rf/dispatch [:set-loading-table false]))))))

(defn share
  [intervals]
  (let [filename (str "declaratie_" (t/today))
        mimetype "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"]
    (rf/dispatch [:set-loading-table true])
    (-> intervals
        (export/intervals->xlsx)
        (.then (fn [contents]
                 (let [url (str "data:" mimetype ";base64," contents)]
                   (rf/dispatch [:set-loading-table false])
                   (.open Share #js {:filename filename
                                     :url url}))))
        (.catch (fn [error] (rf/dispatch [:set-error-message (.-message error)]))))))

(defn header []
  (let [intervals (rf/subscribe [:get-intervals])
        running? (rf/subscribe [:stopwatch-running?])]
    (fn []
      [:> (.-Header rnp/Appbar)
       [:> (.-Action rnp/Appbar) {:icon "menu"
                                  :on-press #((:dispatch @ref/navigation) (.toggleDrawer DrawerActions))}]
       [:> (.-Content rnp/Appbar)
        {:title "Tabel"}]
       [:> (.-Action rnp/Appbar) {:icon "content-save"
                                  :disabled (empty? @intervals)
                                  :on-press #(reset! dialog-visible? true)}]
       [:> (.-Action rnp/Appbar) {:icon "share-variant"
                                  :disabled (empty? @intervals)
                                  :on-press #(share @intervals)}]
       (if @running?
         [stopwatch/icon])])))

(defn data-table []
  (let [dark-theme? (rf/subscribe [:dark-theme?])
        intervals (rf/subscribe [:get-intervals])]
    (fn []
      (let [header ["Datum" "Dag" "Feestdag" "Bereikbaar" "Gewerkt"]
            rows (map-indexed (fn [index item] (assoc item :index index)) @intervals)
            width [100 100 100 100 100]]
        [:> rn/ScrollView
         {:horizontal true}
         [:> rn/View
          {:style (styles/table-container @dark-theme?)}
          [:> Table
           [:> Row
            {:style (styles/header-row @dark-theme?)
             :text-style (styles/header-text @dark-theme?)
             :data (clj->js header)
             :width-arr (clj->js width)}]]
          [:> rn/ScrollView
           [:> Table
            (doall
             (for [row rows]
               (let [interval (or (:standby row)
                                  (:worked row))
                     standby? (some? (:standby row))
                     worked? (some? (:worked row))
                     start (t/beginning interval)
                     end (t/end interval)
                     date (time/format-date start "dd-MM-yyyy")
                     weekday (time/format-date start "EEEE")
                     holiday? (holiday/holiday? (t/date start))
                     range (str (time/format-date start "HH:mm")
                                "-"
                                (if (and (= (t/hour end) 0)
                                         (= (t/minute end) 0))
                                  "24:00"
                                  (time/format-date end "HH:mm")))]
                 ^{:key (:index row)}
                 [:> TableWrapper
                  {:style (styles/table-row @dark-theme?)}
                  [:> Cell
                   {:style (styles/table-cell {:width (nth width 0)})
                    :text-style (styles/table-text @dark-theme?)
                    :data date}]
                  [:> Cell
                   {:style (styles/table-cell {:width (nth width 1)})
                    :text-style (styles/table-text @dark-theme?)
                    :data weekday}]
                  [:> Cell
                   {:style (styles/table-cell {:width (nth width 2)})
                    :data (r/as-element [:> MaterialCommunityIcons
                                         {:name (when holiday? "check")
                                          :color (styles/icon-color @dark-theme?)}])}]
                  [:> Cell
                   {:style (styles/table-cell {:width (nth width 3)})
                    :text-style (styles/table-text @dark-theme?)
                    :data (when standby? range)}]
                  [:> Cell
                   {:style (styles/table-cell {:width (nth width 4)})
                    :text-style (styles/table-text @dark-theme?)
                    :data (when worked? range)}]])))]]]]))))

(defn message-bar []
  [:> rnp/Snackbar
   {:visible @message
    :on-dismiss #(reset! message nil)}
   @message])

(defn error-bar []
  (let [error-message (rf/subscribe [:get-error-message])]
    (fn []
      [:> rnp/Snackbar
       {:visible (not (empty? @error-message))
        :on-dismiss #(rf/dispatch [:set-error-message ""])}
       @error-message])))

(defn save-dialog []
  (let [intervals (rf/subscribe [:get-intervals])
        storage (rf/subscribe [:get-storage-directory])
        menu-visible? (r/atom false)
        base (r/atom (str "declaratie_" (t/today)))]
    (fn []
      [:> rnp/Portal
       [:> rnp/Dialog
        {:visible @dialog-visible?}
        [:> (.-Title rnp/Dialog) "Opslaan als"]
        [:> (.-Content rnp/Dialog)
         [:> rnp/Subheading "Opslaglocatie"]
         [:> rnp/Menu
          {:visible @menu-visible?
           :on-dismiss #(reset! menu-visible? false)
           :anchor (r/as-element [:> rnp/Button
                                  {:content-style {:flex-direction "row-reverse"
                                                   :justify-content "flex-end"}
                                   :mode "outlined"
                                   :icon (if @menu-visible? "menu-up" "menu-down")
                                   :uppercase false
                                   :on-press #(reset! menu-visible? true)}
                                  (case @storage
                                    "internal" "Interne opslag"
                                    "downloads" "Downloads"
                                    @storage)])}
          [:> (.-Item rnp/Menu)
           {:title "Interne opslag"
            :icon "folder-home"
            :on-press (fn []
                        (reset! menu-visible? false)
                        (rf/dispatch [:set-storage-directory "internal"]))}]
          [:> (.-Item rnp/Menu)
           {:title "Downloads"
            :icon "folder-download"
            :on-press (fn []
                        (reset! menu-visible? false)
                        (rf/dispatch [:set-storage-directory "downloads"]))}]
          [:> rnp/Divider]
          [:> (.-Item rnp/Menu)
           {:title "Bladeren"
            :icon "folder"
            :on-press (fn []
                        (reset! menu-visible? false)
                        (-> (.pickDirectory RNDocumentPicker)
                            (.then #(rf/dispatch [:set-storage-directory %]))
                            (.catch (fn [error] (rf/dispatch [:set-error-message (.-message error)])))))}]]
         [:> rnp/Subheading "Bestandsnaam"]
         [:> rnp/TextInput
          {:mode "outlined"
           :default-value @base
           :on-change-text #(reset! base %)
           :right (r/as-element [:> (.-Affix rnp/TextInput) {:text ".xlsx"}])}]]
        [:> (.-Actions rnp/Dialog)
         [:> rnp/Button
          {:on-press #(reset! dialog-visible? false)}
          "Annuleren"]
         [:> rnp/Button
          {:on-press (fn []
                       (let [filename (str @base ".xlsx")
                             encoding "base64"]
                         (rf/dispatch [:set-loading-table true])
                         (-> @intervals
                             (export/intervals->xlsx)
                             (.then (fn [contents]
                                      (rf/dispatch [:set-loading-table false])
                                      (write-file @storage filename contents encoding)))
                             (.catch (fn [error] (rf/dispatch [:set-error-message (.-message error)])))))
                       (reset! dialog-visible? false))}
          "Opslaan"]]]])))

(defn screen [props]
  (let [dark-theme? (rf/subscribe [:dark-theme?])
        loading? (rf/subscribe [:loading-table?])
        top (-> (.get rn/Dimensions "window")
                (.-height)
                (- 48) ; size of large indicator
                (/ 2))
        left (-> (.get rn/Dimensions "window")
                 (.-width)
                 (- 48) ; size of large indicator
                 (/ 2))]
    (fn [props]
      [:> SafeAreaView
       {:style {:flex 1}}
       [:> rn/StatusBar
        {:background-color (get-in (if @dark-theme? styles/dark-theme styles/default-theme) [:colors :primary])
         :status-bar-style (if @dark-theme? "dark-content" "light-content")}]
       [header]
       [data-table]
       [message-bar]
       [error-bar]
       [save-dialog]
       (when @loading?
         [:> rnp/ActivityIndicator
          {:style {:position "absolute"
                   :top top
                   :left left}
           :size "large"}])])))
