(ns declaratie.interval
  (:require [tick.core :as t]
            [tick.alpha.interval :as t.i]
            [declaratie.time :as time]))

(defn event->interval
  "Return a `tick` interval of the event, or nil."
  [event]
  (let [{:keys [startDate endDate allDay]} event
        start (if allDay (time/utc->instant (t/instant startDate)) (t/instant startDate))
        end (if allDay (time/utc->instant (t/instant endDate)) (t/instant endDate))]
    (cond
      (t/= start end) nil
      (t/< start end) (t.i/new-interval start end)
      (t/> start end) (t.i/new-interval end start))))

(defn call->interval
  "Return a `tick` interval of the call, or nil."
  [call]
  (let [seconds (:duration call)]
    (if (zero? seconds)
      nil
      (let [start (time/timestamp->instant (js/parseInt (:timestamp call)))
            duration (t/new-duration seconds :seconds)]
        (t.i/extend start duration)))))

(defn round-off-by-quarter
  "Round the beginning of an interval off by quarters while preserving the duration."
  [interval]
  (let [new-start (-> interval
                      t/beginning
                      time/instant->timestamp
                      (time/round-off-by 15)
                      time/timestamp->instant)]
    (t.i/extend new-start (t/duration interval))))

(defn round-up-to-half-hour
  "Round the duration of an interval up to half hours while preserving the beginning."
  [interval]
  (let [new-duration (-> interval
                         t/duration
                         t/millis
                         (time/round-up-to 30)
                         (t/new-duration :millis))]
    (t.i/extend (t/beginning interval) new-duration)))

(defn concur?
  "Returns true if interval `x` overlaps interval `y`."
  [x y]
  (and (some? x)
       (some? y)
       (if (t.i/concur? x y) true false)))

(defn union
  "Return a vector of time-ordered disjoint intervals from a sequence of concurrent intervals."
  [intervals]
  (loop [intervals (sort-by t/beginning intervals)
         result []]
    (if (empty? intervals) ; done
      result
      (let [current (first intervals)
            previous (last result)]
        (cond
          (or (empty? result)
              (t.i/disjoint? previous current))
          (recur (rest intervals)
                 (conj result current))
          (t.i/concur? previous current)
          (recur (rest intervals)
                 (-> result
                     (pop)
                     (conj (t.i/bounds previous current)))))))))

(defn subtract
  "Subtract the intervals `y` from interval `x` and returns a vector with the
  resulting intervals."
  ([x y]
   (cond
     (nil? x) []
     (nil? y) [x]
     :else
     (case (t.i/relation y x)
       (:equals :contains y x) []
       (:precedes :meets :met-by :preceded-by) [x]
       (:overlaps :starts) [(t.i/new-interval (t/end y) (t/end x))]
       :during [(t.i/new-interval (t/beginning x) (t/beginning y))
                (t.i/new-interval (t/end y) (t/end x))]
       (:finishes :overlapped-by) [(t.i/new-interval (t/beginning x) (t/beginning y))])))
  ([x y & more]
   (loop [intervals more
          result (subtract x y)]
     (if (empty? intervals) ; done
       result
       (let [interval (first intervals)]
         (recur (rest intervals)
                (->> result
                     (map #(subtract % interval))
                     (apply concat))))))))

(defn split-at-midnight
  "Return a vector of intervals, split at midnight."
  [intervals]
  (loop [intervals (sort-by t/beginning intervals)
         result []]
    (if (empty? intervals) ; done
      result
      (let [current (first intervals)
            midnight (-> current
                         t/beginning
                         t/date
                         t/inc
                         (t/at (t/midnight))
                         t/instant)
            [before-midnight after-midnight] (t.i/split-interval current midnight)]
        (recur (if after-midnight
                 (conj (rest intervals) after-midnight)
                 (rest intervals))
               (conj result before-midnight))))))
