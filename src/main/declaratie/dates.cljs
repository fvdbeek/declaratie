(ns declaratie.dates
  (:require
   [clojure.string :as str]
   [cognitect.transit :as transit]
   [tick.core :as t]))

(defn write-date
  "Represent date in ISO 8601 format string."
  [d]
  (str d))

(defn read-date
  "Read ISO 8601 string to date."
  [s]
  (t/date s))

(defn write-time
  "Represent time in ISO 8601 format string."
  [d]
  (str d))

(defn read-time
  "Read ISO 8601 string to time."
  [s]
  (t/time s))

(defn write-date-time
  "Represent date-time in ISO 8601 format string."
  [d]
  (str d))

(defn read-date-time
  "Read ISO 8601 string to date-time."
  [s]
  (t/date-time s))

(defn write-zoned-date-time
  "Represent zoned-date-time in ISO 8601 format string."
  [d]
  (str d))

(defn read-zoned-date-time
  "Read ISO 8601 string to zoned-date-time."
  [s]
  (t/zoned-date-time s))

(defn write-instant
  "Represent instant in string."
  [t]
  (str t))

(defn read-instant
  "Read string to instant."
  [s]
  (t/instant s))

;; (defn write-duration
;;   "Represent duration in string."
;;   [t]
;;   (str t))

;; (defn read-duration
;;   "Read string to duration."
;;   [s]
;;   (t/new-duration s))

;; (defn write-period
;;   "Represent period in string."
;;   [t]
;;   (str t))

;; (defn read-period
;;   "Read string to period."
;;   [s]
;;   (t/new-period s))

(defn write-year
  "Represent year in string."
  [t]
  (str t))

(defn read-year
  "Read string to year."
  [s]
  (t/year s))

(defn write-year-month
  "Represent year-month in string."
  [t]
  (str t))

(defn read-year-month
  "Read string to year-month."
  [s]
  (t/year-month s))

(defn write-month
  "Represent month in string."
  [t]
  (str t))

(defn read-month
  "Read string to month."
  [s]
  (t/month s))

(defn write-day-of-week
  "Represent day-of-week in string."
  [t]
  (str t))

(defn read-day-of-week
  "Read string to day-of-week."
  [s]
  (t/day-of-week s))

(def writers
  {java.time.LocalDate (transit/write-handler (constantly "date") write-date)
   java.time.LocalTime (transit/write-handler (constantly "time") write-time)
   java.time.LocalDateTime (transit/write-handler (constantly "date-time") write-date-time)
   java.time.ZonedDateTime (transit/write-handler (constantly "zoned-date-time") write-zoned-date-time)
   java.time.Instant (transit/write-handler (constantly "instant") write-instant)
   java.time.Year (transit/write-handler (constantly "year") write-year)
   java.time.YearMonth (transit/write-handler (constantly "year-month") write-year-month)
   java.time.Month (transit/write-handler (constantly "month") write-month)
   java.time.DayOfWeek (transit/write-handler (constantly "day-of-week") write-day-of-week)})

(def readers
  {"date" (transit/read-handler read-date)
   "time" (transit/read-handler read-time)
   "date-time" (transit/read-handler read-date-time)
   "zoned-date-time" (transit/read-handler read-zoned-date-time)
   "instant" (transit/read-handler read-instant)
   "year" (transit/read-handler read-year)
   "year-month" (transit/read-handler read-year-month)
   "month" (transit/read-handler read-month)
   "day-of-week" (transit/read-handler read-day-of-week)})
