(ns declaratie.agenda
  (:require
   [goog.object :as gobj]
   [reagent.core :as r]
   [re-frame.core :as rf]
   [clojure.string :as str]
   [tick.core :as t]
   [tick.alpha.interval :as t.i]
   ["react" :as react]
   ["react-native" :as rn]
   ["react-native-bidirectional-infinite-scroll" :refer [FlatList]]
   ["react-native-calendars" :refer [Calendar WeekCalendar LocaleConfig]]
   ["react-native-vector-icons/MaterialCommunityIcons" :refer [default] :rename {default MaterialCommunityIcons}]
   ["react-native-paper" :as rnp]
   ["react-native-send-intent" :as rnsi]
   ["react-native-safe-area-context" :refer [SafeAreaView]]
   ["@react-navigation/native" :refer [DrawerActions]]
   ["xdate" :as XDate]
   [declaratie.ref :as ref]
   [declaratie.stopwatch :as stopwatch]
   [declaratie.styles :as styles]
   [declaratie.time :as time]))

(set! (.. LocaleConfig -locales -nl) (clj->js {:monthNames time/months
                                               :monthNamesShort time/shortmonths
                                               :dayNames time/weekdays
                                               :dayNamesShort time/shortweekdays
                                               :today "Vandaag"}))

(set! (.. LocaleConfig -defaultLocale) "nl")

(def searchbar-visible? (r/atom false))

(def context-menu-visible? (r/atom false))

(def context-menu-coordinates (r/atom {:x 0 :y 0}))

(def dialog-visible? (r/atom false))

(def viewable-items (atom nil))

(def viewability-config #js {:minimumViewTime 500
                             :waitForInteraction true
                             :itemVisiblePercentThreshold 100})

(defn get-index
  "Return the index of `events` of which the startDate is on, just after or just before `date`."
  [date events]
  (let [pred (fn [[index event]]
               (-> event
                   :interval
                   t/beginning
                   t/date
                   (t/>= date)))
        indexed (map-indexed (fn [index event] [index event]) events)]
    (or (ffirst (filter pred indexed))
        (first (last indexed)))))

(defn get-item-layout
  [data index]
  (let [item-height 72
        separator-height 0.5
        row-height (+ item-height separator-height)]
    #js {:length row-height
         :offset (* index row-height)
         :index index}))

(defn add-month [months]
  (rf/dispatch [:update-current-date months :months])
  (when @ref/calendar
    (.addMonth ^js/Calendar @ref/calendar months)))

(defn update-month [date]
  (rf/dispatch [:set-current-date date])
  (when @ref/calendar
    (let [xdate (-> date
                    (t/at (t/time "00:00"))
                    t/inst
                    XDate)]
      (.updateMonth ^js/Calendar @ref/calendar xdate))))

(defn fetch-previous-events []
  (when goog.DEBUG (js/console.log "Fetching previous events..."))
  (js/Promise.resolve (rf/dispatch [:extend-calendar-bounds -1 :weeks])))

(defn fetch-next-events []
  (when goog.DEBUG (js/console.log "Fetching next events..."))
  (js/Promise.resolve (rf/dispatch [:extend-calendar-bounds 1 :weeks])))

(defn on-month-change
  "Callback function on changing the calendar month."
  [props]
  (let [date-string (gobj/get props "dateString")
        year (gobj/get props "year")
        month (gobj/get props "month")
        date (t/date date-string)
        year-month (t/new-year-month year month)
        month-bounds (t.i/bounds year-month)
        current-bounds (rf/subscribe [:get-calendar-bounds])]
    (when goog.DEBUG (js/console.log "Month changed to" (str date)))
    (case (t.i/relation date @current-bounds)
      :precedes
      (let [days (-> (t.i/new-interval (t/beginning month-bounds) (t/beginning @current-bounds))
                     (t/duration)
                     (t/days))]
        (rf/dispatch [:extend-calendar-bounds (- days) :days]))
      :preceded-by
      (let [days (-> (t.i/new-interval (t/end @current-bounds) (t/end month-bounds))
                     t/duration
                     t/days)]
        (rf/dispatch [:extend-calendar-bounds days :days]))
      :during)))

(defn on-day-press
  "Callback function on pressing a day in the calendar."
  [date]
  (let [events (rf/subscribe [:get-events])
        day (gobj/get date "dateString")
        date (t/date day)]
    (rf/dispatch [:set-current-date date])
    (when-not (empty? @events)
      (.scrollToIndex ^js/FlatList @ref/calendar-list #js {:index (get-index date @events)}))))

(defn on-today-press []
  (let [events (rf/subscribe [:get-events])
        date (t/today)]
    (update-month date)
    (when-not (empty? @events)
      (.scrollToIndex ^js/FlatList @ref/calendar-list #js {:index (get-index date @events)}))))

(defn on-viewable-items-changed [props]
  (let [current-date (rf/subscribe [:get-current-date])
        items (-> props
                  (gobj/get "viewableItems")
                  (js->clj :keywordize-keys true))]
    (reset! viewable-items items)
    (when-not (empty? items)
      (let [first-date (-> items
                           first
                           (get-in [:item :interval])
                           t/beginning
                           t/date)]
        (update-month first-date)))))

(defn context-menu []
  [:> rnp/Menu
   {:visible @context-menu-visible?
    :on-dismiss #(reset! context-menu-visible? false)
    :anchor (clj->js @context-menu-coordinates)}
   [:> (.-Item rnp/Menu)
    {:title "Alles selecteren"
     :on-press (fn []
                 (reset! context-menu-visible? false)
                 (doseq [item @viewable-items]
                   (let [event (:item item)]
                     (rf/dispatch [:select-event event]))))}]
   [:> (.-Item rnp/Menu)
    {:title "Niets selecteren"
     :on-press (fn []
                 (reset! context-menu-visible? false)
                 (reset! dialog-visible? true))}]])

(defn dialog []
  [:> rnp/Portal
   [:> rnp/Dialog
    {:visible @dialog-visible?}
    [:> (.-Title rnp/Dialog) "Niets selecteren?"]
    [:> (.-Content rnp/Dialog)
     [:> rnp/Paragraph
      "Alle activiteiten en gesprekken zullen ook gedeselecteerd worden. Er zal niets worden verwijderd."]]
    [:> (.-Actions rnp/Dialog)
     [:> rnp/Button
      {:on-press #(reset! dialog-visible? false)}
      "Annuleren"]
     [:> rnp/Button
      {:on-press (fn []
                   (rf/dispatch [:clear-events])
                   (reset! dialog-visible? false))}
      "OK"]]]])

(defn checkbox [event]
  (let [selected? (rf/subscribe [:event-selected? event])]
    (fn [event]
      [:> rnp/Checkbox
       {:status (if @selected? "checked" "unchecked")
        :on-press #(rf/dispatch [:toggle-event event])}])))

(defn render-item [props]
  (let [index (gobj/get props "index")
        event (-> props
                  (gobj/get "item")
                  (js->clj :keywordize-keys true))
        {title :title
         all-day? :allDay
         recurrent? :recurrence
         interval :interval
         calendar-id :calendarId} event
        events (rf/subscribe [:get-events])
        calendar (rf/subscribe [:get-calendar calendar-id])
        start (t/beginning interval)
        end (t/end interval)
        current-date (t/date start)
        previous-date (some-> @events
                              (nth (- index 1) nil)
                              :interval
                              t/beginning
                              t/date)]
    (fn [props]
      [:> rn/View
       {:style {:height 72
                :flex 1
                :flex-direction "row"}}
       [:> rn/View
        {:style {:flex 1
                 :align-items "center"
                 :padding-horizontal 8}}
        (when-not (= current-date previous-date)
          [:<>
           [:> rnp/Caption
            (-> start
                t/day-of-week
                t/int
                (rem 7) ; Sunday is at index 0 instead of 7
                time/shortweekdays
                str/capitalize)]
           [:> rnp/Headline
            (-> start
                t/day-of-month
                str)]])]
       [:> (.-Item rnp/List)
        {:style {:flex 7}
         :on-press #(rf/dispatch [:toggle-event event])
         :on-long-press (fn [e]
                          (let [x (gobj/getValueByKeys e #js ["nativeEvent" "pageX"])
                                y (gobj/getValueByKeys e #js ["nativeEvent" "pageY"])]
                            (reset! context-menu-coordinates {:x x :y y})
                            (reset! context-menu-visible? true)))
         :title title
         :description (if all-day?
                        (str (time/format-date start "dd-MM-yyyy")
                             " "
                             "Hele dag")
                        (str (time/format-date start "dd-MM-yyyy")
                             " "
                             (time/format-date start "HH:mm")
                             " - "
                             (time/format-date end "HH:mm")))
         :description-number-of-lines 1
         :left #(r/as-element [:> rn/View
                               {:style {:justify-content "center"}}
                               [:> rn/View
                                {:style (styles/calendar-circle (:color @calendar))}]])
         :right (fn [props] (r/as-element [:> rn/View
                                           {:style {:flex-direction "row"
                                                    :align-items "center"}}
                                           (when all-day?
                                             [:> (.-Icon rnp/List)
                                              {:icon "cached"}])
                                           [checkbox event]]))}]])))

(defn searchbar []
  (let [query (r/atom "")
        calendar-visible? @(rf/subscribe [:calendar-visible?])]
    (fn []
      (rf/dispatch [:set-calendar-visible false])
      [:> rnp/Searchbar
       {:auto-capitalize "none"
        :placeholder "Zoeken in agenda"
        :default-value @query
        :on-change-text #(reset! query %)
        :on-end-editing #(rf/dispatch [:set-query @query])
        :icon "arrow-left"
        :on-icon-press (fn []
                         (rf/dispatch [:set-query nil])
                         (reset! searchbar-visible? false)
                         (rf/dispatch [:set-calendar-visible calendar-visible?]))}])))

(defn header []
  (let [current-date (rf/subscribe [:get-current-date])
        calendar-visible? (rf/subscribe [:calendar-visible?])
        filter-menu-visible? (r/atom false)
        event-filter (rf/subscribe [:get-event-filter])
        running? (rf/subscribe [:stopwatch-running?])]
    (fn []
      [:> (.-Header rnp/Appbar)
       [:> (.-Action rnp/Appbar)
        {:icon "menu"
         :on-press #((:dispatch @ref/navigation) (.toggleDrawer DrawerActions))}]
       [:> rnp/Button
        ;; Use the style of Appbar.Content for correct alignment of the header
        ;; children, but leave out the horizontal padding to create enough space
        ;; for the button contents
        {:style {:flex 1
                 :margin-left 8
                 :align-items "center"}
         :content-style {:flex-direction "row-reverse"}
         :uppercase false
         :color "white"
         :icon (if @calendar-visible? "menu-up" "menu-down")
         :on-press #(rf/dispatch [:toggle-calendar-visible])}
        (some-> @current-date
                (t/at "00:00")
                (time/format-date "MMMM yyyy")
                str/capitalize)]
       [:> (.-Action rnp/Appbar)
        {:icon "magnify"
         :on-press #(reset! searchbar-visible? true)}]
       [:> rnp/Menu
        {:anchor (r/as-element [:> rnp/IconButton
                                {:icon "filter"
                                 :color "white"
                                 :on-press #(swap! filter-menu-visible? not)}])
         :visible  @filter-menu-visible?
         :on-dismiss #(reset! filter-menu-visible? false)}
        [:> (.-Group rnp/RadioButton)
         {:on-value-change (fn [value]
                             (rf/dispatch [:set-event-filter value])
                             (reset! filter-menu-visible? false))
          :value @event-filter}
         [:> (.-Item rnp/RadioButton)
          {:label "Alle"
           :value false}]
         [:> (.-Item rnp/RadioButton)
          {:label "Geselecteerd"
           :value true}]]]
       [:> (.-Action rnp/Appbar)
        {:icon "refresh"
         :on-press #(rf/dispatch [:load-events])}]
       (if @running?
         [stopwatch/icon])])))

(defn error-bar []
  (let [error-message (rf/subscribe [:get-error-message])]
    (fn []
      [:> rnp/Snackbar
       {:visible (not (empty? @error-message))
        :on-dismiss #(rf/dispatch [:set-error-message ""])}
       @error-message])))

(defn calendar []
  (let [marked (rf/subscribe [:get-marked])
        dark-theme? (rf/subscribe [:dark-theme?])]
    (fn []
      [:> Calendar
       {:ref #(reset! ref/calendar %)
        :key @dark-theme? ; hack to re-render the calendar when the theme changes
        :theme (styles/calendar @dark-theme?)
        :marked-dates (clj->js @marked)
        :marking-type "dot"
        :on-day-press on-day-press
        :on-month-change on-month-change
        :hide-arrows false
        :render-arrow (fn [direction] (r/as-element [:> MaterialCommunityIcons
                                                     {:name (case direction
                                                              "left" "chevron-left"
                                                              "right" "chevron-right")
                                                      :color (get-in (if dark-theme? styles/dark-theme styles/default-theme) [:colors :primary])
                                                      :size 24}]))
        :on-press-arrow-left #(add-month -1)
        :on-press-arrow-right #(add-month 1)
        :hide-extra-days false
        :first-day 1
        :hide-day-names false
        :show-week-numbers false
        :disable-all-touch-events-for-disabled-days false
        :render-header #(r/as-element
                         [:> rnp/Button
                          {:icon "calendar-today"
                           :uppercase false
                           :on-press on-today-press}
                          "Vandaag"])
        :enable-swipe-months true}])))

;; Define `viewabilityConfig` in the constructor to avoid following error:
;; "Error: Changing viewabilityConfig on the fly is not supported" (see
;; https://github.com/facebook/react-native/issues/17408)
(defn calendar-list []
  (let [events (rf/subscribe [:get-events])
        current-date (rf/subscribe [:get-current-date])
        keyword-fn (fn [x] (let [ns (namespace x)
                                 name (name x)]
                             (if ns
                               (str ns "/" name)
                               name)))]
    (r/create-class
     {:constructor (fn [this props]
                     (set! (.-viewabilityConfig ^js/FlatList this) viewability-config))
      :reagent-render
      (fn []
        [:> FlatList
         {:ref #(reset! ref/calendar-list %)
          :data (-> @events
                    (clj->js :keyword-fn keyword-fn))
          :render-item #(r/as-element [render-item %])
          :ItemSeparatorComponent #(r/as-element [:> rnp/Divider])
          :get-item-layout get-item-layout
          :initial-scroll-index (get-index @current-date @events)
          :on-start-reached #(fetch-previous-events)
          :on-end-reached #(fetch-next-events)
          :on-viewable-items-changed on-viewable-items-changed
          :on-start-reached-threshold 72
          :on-end-reached-threshold 72
          :show-default-loading-indicators false}])})))

(defn screen [props]
  (let [dark-theme? (rf/subscribe [:dark-theme?])
        loading? (rf/subscribe [:loading-events?])
        calendar-visible? (rf/subscribe [:calendar-visible?])
        calendars (rf/subscribe [:get-calendars])
        events (rf/subscribe [:get-all-events])
        top (-> (.get rn/Dimensions "window")
                (.-height)
                (- 48) ; size of large indicator
                (/ 2))
        left (-> (.get rn/Dimensions "window")
                 (.-width)
                 (- 48) ; size of large indicator
                 (/ 2))]
    (fn [props]
      [:> SafeAreaView
       {:style {:flex 1}}
       [:> rn/StatusBar
        {:background-color (styles/statusbar-color @dark-theme?)
         :status-bar-style (if @dark-theme? "dark-content" "light-content")}]
       (if @searchbar-visible?
         [searchbar]
         [header])
       (if @calendar-visible?
         [:> rnp/Surface
          {:style {:flex-basis "auto"
                   :flex-grow 0}
           :elevation 2}
          [calendar]])
       [:> rn/View
        {:style {:flex-basis "auto"
                 :flex-grow 1}}
        [calendar-list]]
       [context-menu]
       [dialog]
       [error-bar]
       (when @loading?
         [:> rnp/ActivityIndicator
          {:style {:position "absolute"
                   :top top
                   :left left}
           :size "large"}])])))
