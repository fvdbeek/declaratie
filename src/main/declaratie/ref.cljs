(ns declaratie.ref)

;; navigation ref clojure.core/atom and not a reagent.core/atom. We use a normal
;; Clojure atom because refs never change during the lifecycle of a component
;; and if we used a reagent atom, it would cause an unnecessary re-render when
;; the ref callback mutates the atom.
(def navigation (atom nil))
(def calendar (atom nil))
(def calendar-list (atom nil))
